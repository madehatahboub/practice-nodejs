const express = require("express");
const userRouter = require('./routes/user')
const cors = require("cors");
require('./db/mongoose');

const app = express();
const port = process.env.PORT || 4000;

app.use(express.json());
app.use(cors())
app.use(userRouter);

app.listen(port, () => {
    console.log(`Server is running on port ${port}`);
});
