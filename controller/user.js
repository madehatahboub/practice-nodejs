const User = require("../model/user");
const cache = require('../services/cache')

const addNewUser = async(req, res) => {
    const newUser = new User(req.body);
    try {
        await newUser.save();
        res.status(201).send(newUser);
    } catch (error) {
        res.status(400).send(error);
    }
}

const getAllUsers = async(req, res) => {
    //res.send("API is working properly");
     try {
         let cachedData = cache.get("users");
         if(cachedData != undefined){
            res.send(cachedData);
            console.log('form the cache')
            return;
         }
         const users = await User.find({});
         cache.set("users" , users, 60);
         res.send(users);
         console.log('form the server')


     } catch (error) {
         res.status(500).send();
     }
 }

const getUserById =  async(req, res) => {
    const _id = req.params.id;
    try {
        const user = await User.findById(_id);
        if (!user) {
            res.status(404).send();
        }
    
        res.send(user);
    } catch (error) {
        res.status(500).send();
    }
}

const updateUser = async(req,res) => {
    const _id = req.params.id;
    const updatedUser = req.body;
    const updatedFields = Object.keys(updatedUser);

    try {
        const user = await User.findById(_id);
        updatedFields.forEach((field) => {
            user[field] = updatedUser[field];
        })
        await user.save();
        if (!user) {
            return res.status(404).send();
        }
        res.send(user);

    } catch (error) {
        res.status(400).send(error);
    }
}

const deleteUser = async(req,res) => {
    const _id = req.params.id;
    try {
        const user = await User.findByIdAndDelete(_id);
        if (!user) {
            return res.status(404).send();
        }
        
        res.send(user);
    } catch (error) {
        res.status(500).send();
    }
}

 module.exports = {
    addNewUser,
    getAllUsers,
    getUserById,
    updateUser,
    deleteUser
};