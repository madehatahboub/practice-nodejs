var mongoose = require("mongoose");

const userSchema = mongoose.Schema({
    name: {
        type: String
    },
    age: {
        type: Number
    }
});

const User = mongoose.model('users', userSchema); // users represents the name of the collection in mongodb
module.exports = User;