const express = require("express");
const User = require("../model/user");
const router = new express.Router();

const userController = require("../controller/user")

router.post('/addUser',userController.addNewUser);

router.get('/users', userController.getAllUsers);

router.get('/users/:id',userController.getUserById);

router.delete('/deleteUser/:id',userController.deleteUser)

router.patch('/updateUser/:id',userController.updateUser);

module.exports = router;